﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace SIMAB_GSI
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
            button1.MouseEnter += new EventHandler(Button1_MouseEnter);
            button1.MouseLeave += new EventHandler(button1_MouseLeave);
            button2.MouseEnter += new EventHandler(Button2_MouseEnter);
            button2.MouseLeave += new EventHandler(button2_MouseLeave);
            button3.MouseEnter += new EventHandler(Button3_MouseEnter);
            button3.MouseLeave += new EventHandler(button3_MouseLeave);
            button4.MouseEnter += new EventHandler(Button4_MouseEnter);
            button4.MouseLeave += new EventHandler(button4_MouseLeave);
            void button1_MouseLeave(object sender, EventArgs e)
            {
                this.button1.BackgroundImage = ((System.Drawing.Image)( Properties.Resources.MARL11));
            }

            void Button1_MouseEnter(object sender, EventArgs e)
            {
                this.button1.BackgroundImage = ((System.Drawing.Image)(Properties.Resources.MARLneon1));
                label1.Show();
            }

            void button2_MouseLeave(object sender, EventArgs e)
            {
                this.button2.BackgroundImage = ((System.Drawing.Image)(Properties.Resources.MARB11));
                label1.hide();
            }

            void Button2_MouseEnter(object sender, EventArgs e)
            {
                this.button2.BackgroundImage = ((System.Drawing.Image)(Properties.Resources.MARBneon));
                label1.Show();
            }

            void button3_MouseLeave(object sender, EventArgs e)
            {
                this.button3.BackgroundImage = ((System.Drawing.Image)(Properties.Resources.MARE11));
                label1.Show();
            }

            void Button3_MouseEnter(object sender, EventArgs e)
            {
                this.button3.BackgroundImage = ((System.Drawing.Image)(Properties.Resources.MAREneno));
                label1.Show();
            }

            void button4_MouseLeave(object sender, EventArgs e)
            {
                this.button4.BackgroundImage = ((System.Drawing.Image)(Properties.Resources.MARF1));
                label1.Show();
            }

            void Button4_MouseEnter(object sender, EventArgs e)
            {
                this.button4.BackgroundImage = ((System.Drawing.Image)(Properties.Resources.MARFneon));
                label1.Show();
            }
        }
    }
    
}
